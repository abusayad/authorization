import { CanActivate, ExecutionContext, forwardRef, Inject, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { User } from "../users/users.entity";
import { Role } from "./roles.enum";
import * as jwt from 'jsonwebtoken'
import { UsersService } from "../users/users.service";
import { Observable } from "rxjs";

@Injectable()
export class RolesGuard implements CanActivate{
    constructor(private readonly reflector: Reflector,
        @Inject(forwardRef(() => UsersService))
        private usersService: UsersService
        ){}

    canActivate(context: ExecutionContext): boolean|  Promise<boolean>{
        //await super.camActivate(conext);
        const roles = this.reflector.get<string[]>('roles', 
            context.getHandler()
        );
        if(!roles){
            return true;
        }
        console.log('requiredRoles:',roles);

        const request  = context.switchToHttp().getRequest();
         request.user = jwt.decode(request.headers.authorization.split(' ')[1]);
         const user: User = request.user;
         
        return this.usersService.readOne(user.id).then(
            (user: User)=>{
                const hasRole = () => roles.indexOf(user.role) > -1;
                let hasPermission: boolean = false;

                if(hasRole()){
                    hasPermission = true;
                }
                return user && hasPermission;
            }
        )
        
        
        
        
    }
}