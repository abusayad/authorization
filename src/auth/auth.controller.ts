import { Body, Controller, Post } from '@nestjs/common';
import { User } from 'src/users/users.entity';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService){}

    @Post('login')
    async login(@Body() user: User): Promise<any>{
        return await this.authService.login(user);
    }

    @Post('register')
    async register(@Body() user: User): Promise<any>{
        return await this.authService.register(user);
    }
}
