import { HttpStatus, Injectable } from '@nestjs/common';
import { UsersDto } from 'src/users/users.dto';
import { User } from 'src/users/users.entity';
import { UsersService } from 'src/users/users.service';
import * as jwt from 'jsonwebtoken'

@Injectable()
export class AuthService {
    constructor( 
        private readonly usersService: UsersService
    ){}

    private async validateUser(userData: User){
        return await this.usersService.findByEmail(userData.email);
    }
    public async login(user: User): Promise<any | {status: number}>{
        return await this.validateUser(user).then((userData)=>{
            if(!userData){
                return {
                    statusCode: HttpStatus.BAD_REQUEST,
                    message: 'invalid email/password'
                }
            }

            const payload = {id: userData.id, userName: userData.name, role: userData.role, shop: userData.shops};
            const accessToken = jwt.sign(payload, process.env.SECRET, {
                expiresIn: process.env.EXPIRE
            });

            return {
                statusCode: HttpStatus.OK,
                accessToken: accessToken,
                expires_in: process.env.EXPIRE,
                data: payload
            }
        })
    }

    public async register(user: User): Promise<any>{
        return await this.usersService.create(user);
    }
}
