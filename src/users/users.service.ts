import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersDto } from './users.dto';
import { User } from './users.entity';
import { otp } from './users.otp';
import { transport } from './users.transporter';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>
    ){}

    async showAll(){
        return await this.userRepository.find({ relations: ['shops'] });
    }

    async create(userData: UsersDto){
        
        
        let email= userData.email;
        
        let info = await transport.sendMail({
            from: 'abusayad@simecsystem.com', // sender address
            to: email, // list of receivers
            subject: "Otp for reg is:", // Subject line
            html: "<h3>OTP for account verification is </h3>"  + "<h1 style='font-weight:bold;'>" + otp +"</h1>", // html body
          });
        
          console.log("Message sent: %s", info);
          // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
          const user = await this.userRepository.create(userData);
          await this.userRepository.save(user);
        return user;
    }

    async findByEmail(email: string){
        return this.userRepository.findOne({
            where: {
                email
            }
        })
    }

    async readOne(id: number){
        return await this.userRepository.findOne({
            where: {
                id: id
            }
        })
    }

    async update(id: number, userData: Partial<UsersDto>){
        await this.userRepository.update({id}, userData);
        return this.userRepository.findOne({id});
    }

    async delete(id: number){
        await this.userRepository.delete({id});
        return {deleted: true}
    }
}
