import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, UseGuards, UseInterceptors } from '@nestjs/common';
import { Role } from '../role/roles.enum';
import { AuthGuard } from '../auth/auth.guard';
import { UsersDto } from './users.dto';
import { UsersService } from './users.service';
import { User } from './users.entity';
import { RolesGuard } from 'src/role/roles.guard';
import { hasRoles } from 'src/role/roles.decorator';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService){}

    
    @UseGuards(new AuthGuard(), RolesGuard)
    @hasRoles(Role.ADMIN)
    @Get()
    async showAllUsers(){
        return{
            statusCode: HttpStatus.OK,
            data: await this.usersService.showAll()
        }
    }

    @Post()
    async createUsers(@Body() userData: UsersDto){
        return{
            statusCode: HttpStatus.CREATED,
            message: 'User added successfully',
            data: await this.usersService.create(userData)
        }
    }

    @Get(':id')
    @UseGuards(new AuthGuard())
    async readUser(@Param('id') id: number ){
        return{
            statusCode: HttpStatus.OK,
            data: await this.usersService.readOne(id)
        }
    }

    @Put(':id')
    @UseGuards(new AuthGuard())
    async updateUser(@Param('id') id: number, @Body() userData: Partial<UsersDto>){
        return {
            statusCode: HttpStatus.OK,
            data: await this.usersService.update(id, userData)
        }
    }

    @Delete(':id')
    @UseGuards(new AuthGuard())
    async deleteUser(@Param('id') id: number){
        await this.usersService.delete(id);
        return {
            statusCode: HttpStatus.OK,
            data: await this.usersService.delete(id)
        }
    }


}
