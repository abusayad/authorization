import * as crypto from 'crypto';
import { Role } from '../role/roles.enum';
import { Shop } from '../shops/shops.entity'
import { BeforeInsert, Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User{
@PrimaryGeneratedColumn()
id: number;
@Column()
name: string;
@Column()
email: string;
@BeforeInsert()
async hashedPassword(){
    this.password = crypto.createHmac('sha256', this.password).digest('hex');
}
@Column()
password: string;
@Column({type: 'enum', enum: Role, default: Role.USER})
role: Role;

@ManyToMany(() => Shop, shop => shop.users)
@JoinTable({ name: 'user_shop',
            joinColumn: {name: 'user_id'},
            inverseJoinColumn: {name: 'shop_id'}})
shops: Shop[];
}
