import { Shop } from "src/shops/shops.entity";
import { Role } from "../role/roles.enum";

export interface UsersDto{
    id: number,
    name: string,
    email: string,
    password: string,
    role: Role,
    shops: Shop[],
}