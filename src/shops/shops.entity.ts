import { User } from 'src/users/users.entity';
import {  Column, Entity, getConnection, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Shop{
@PrimaryGeneratedColumn()
id: number;
@Column()
name: string;
@ManyToMany(() => User, user => user.shops, { cascade: true })
users: User[];
}
