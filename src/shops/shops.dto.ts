import { User } from "src/users/users.entity";

export interface ShopsDto{
    id: number,
    name: string,
    users: User[]
}