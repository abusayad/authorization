import { Injectable,  } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ShopsDto } from './shops.dto';
import { Shop } from './shops.entity';

@Injectable()
export class ShopsService {
    constructor(
        @InjectRepository(Shop)
        private shopRepository: Repository<Shop>,
    ){}

    async showAll(){
        
        return await this.shopRepository.find({ relations: ['users'] });
    }

    async create(shopData: ShopsDto){
        
    let shop = await this.shopRepository.create(shopData);
    await this.shopRepository.save(shop);
    return shop;
    }

    async readOne(id: number){
        return await this.shopRepository.findOne({
            where: {
                id: id
            }
        })
    }

    async update(id: number, shopData: Partial<ShopsDto>){
        await this.shopRepository.update({id}, shopData);
        return this.shopRepository.findOne({id});
    }

    async delete(id: number){
        await this.shopRepository.delete({id});
        return {deleted: true}
    }
}
