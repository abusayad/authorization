import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/auth/auth.guard';
import { ShopsDto } from './shops.dto';
import { ShopsService } from './shops.service';

@Controller('shops')
export class ShopsController {
    constructor(private shopsService: ShopsService){}

    @Get()
    @UseGuards(new AuthGuard())
    async showAllShops(){
        return{
            statusCode: HttpStatus.OK,
            data: await this.shopsService.showAll()
        }
    }

    @Post()
    async createShops(@Body() shopData: ShopsDto){
        return{
            statusCode: HttpStatus.CREATED,
            message: 'Shop added successfully',
            data: await this.shopsService.create(shopData)
        }
    }

    @Get(':id')
    @UseGuards(new AuthGuard())
    async readShops(@Param('id') id: number ){
        return{
            statusCode: HttpStatus.OK,
            data: await this.shopsService.readOne(id)
        }
    }

    @Put(':id')
    @UseGuards(new AuthGuard())
    async updateShops(@Param('id') id: number, @Body() ShopData: Partial<ShopsDto>){
        return {
            statusCode: HttpStatus.OK,
            data: await this.shopsService.update(id, ShopData)
        }
    }

    @Delete(':id')
    @UseGuards(new AuthGuard())
    async deleteShop(@Param('id') id: number){
        await this.shopsService.delete(id);
        return {
            statusCode: HttpStatus.OK,
            data: await this.shopsService.delete(id)
        }
    }
}
